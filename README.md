# autosd-image-soafee

A SOAFEE compliant image.

SOAFEE's architecture specification: https://architecture.docs.soafee.io/en/latest/

## Geting Started

### Building

Generate a osbuild json file using `osbuild-mpp`:

```sh
osbuild-mpp \
-I manifests/ \
-D image_type='"regular"' \
-D arch='"x86_64"' \
-D distro_name='"cs9"' \
-D target='"qemu"' \
manifests/main.mpp.yml \
outputs/osbuild/main.json
```

Build the image using `osbuild`:

```sh
osbuild \
--store outputs/osbuild/osbuild_store \
--output-directory outputs \
--export qcow2 \
outputs/osbuild/main.json
```

## Running

Run the image using qemu:

```
sudo ./scripts/runvm outputs/qcow2/disk.qcow2
```

## License

[MIT](./LICENSE)
